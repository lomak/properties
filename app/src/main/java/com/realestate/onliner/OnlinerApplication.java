package com.realestate.onliner;

import android.app.Application;
import android.content.Context;

import com.realestate.onliner.injection.component.ApplicationComponent;

import com.realestate.onliner.injection.component.DaggerApplicationComponent;
import com.realestate.onliner.injection.module.ApplicationModule;
import com.realestate.onliner.injection.module.DataModule;
import com.realestate.onliner.utils.AndroidUtils;

public class OnlinerApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    public static OnlinerApplication get(Context context) {
        return (OnlinerApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidUtils.init(this);
    }

    public ApplicationComponent getApplicationComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .dataModule(new DataModule())
                    .build();
        }
        return mApplicationComponent;
    }
}
