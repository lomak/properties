package com.realestate.onliner.network;

import com.realestate.onliner.network.responsebean.PropertiesResponseBean;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DataManager {

    private final RestApi restApi;

    public DataManager(RestApi restApi) {
        this.restApi = restApi;
    }

    public Observable<PropertiesResponseBean> getProperties(int page) {
        return restApi.getProperties(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
