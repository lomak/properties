package com.realestate.onliner.network.responsebean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.realestate.onliner.model.Property;
import com.realestate.onliner.model.PropertyPage;

import java.util.List;

public class PropertiesResponseBean {

    @Expose @SerializedName("apartments") public List<Property> properties;
    @Expose @SerializedName("total") public int total;
    @Expose @SerializedName("page") public PropertyPage page;

}
