package com.realestate.onliner.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIError {

    @Expose @SerializedName("statusCode") public int statusCode;
    @Expose @SerializedName("message") public String message;

    public APIError() {
    }
}