package com.realestate.onliner.network;

public enum ErrorType {
    NETWORK_ERROR, NO_PERMISSION_ERROR, OTHER
}
