package com.realestate.onliner.network;

import com.google.gson.Gson;
import com.realestate.onliner.BuildConfig;
import com.realestate.onliner.network.responsebean.PropertiesResponseBean;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class RestApi {

    private final Gson gson;
    private OnlinerRestService restService;
    private Retrofit retrofit;

    public RestApi(Gson gson) {
        this.gson = gson;

        OkHttpClient.Builder builder = createOkHttpBuilder();
        OkHttpClient client = builder.build();

        retrofit = createRetrofitBuilder(client).build();
        restService = retrofit.create(OnlinerRestService.class);

    }

    private OkHttpClient.Builder createOkHttpBuilder() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .followRedirects(true)
                .followSslRedirects(true);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Accept", "application/json")
                        .header("Content-Type", "application/json")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }

        return builder;
    }

    private Retrofit.Builder createRetrofitBuilder(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
    }

    public Observable<PropertiesResponseBean> getProperties(int page) {
        return restService.getProperties(page);
    }

}
