package com.realestate.onliner.network;

public interface ApiConstants {

    String SERVER_DATE_FORMAT = "yyyy-MM-dd";

    String GET_PROPERTIES = "/apartments";
}
