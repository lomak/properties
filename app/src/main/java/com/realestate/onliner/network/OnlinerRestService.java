package com.realestate.onliner.network;

import com.realestate.onliner.network.responsebean.PropertiesResponseBean;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface OnlinerRestService {

    @GET(ApiConstants.GET_PROPERTIES)
    Observable<PropertiesResponseBean> getProperties(@Query("page") int page);
}

