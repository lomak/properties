package com.realestate.onliner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PropertyPrice {

    @Expose @SerializedName("amount") public double amount;
    @Expose @SerializedName("currency") public String currency;
    @Expose @SerializedName("converted") public PropertyConvertedPrice converted;

}
