package com.realestate.onliner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyLocation {

    @Expose @SerializedName("address") public String address;
    @Expose @SerializedName("user_address") public String userAddress;
    @Expose @SerializedName("latitude") public double latitude;
    @Expose @SerializedName("longitude") public double longitude;

}
