package com.realestate.onliner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyPage {
    @Expose @SerializedName("last") public int last;
    @Expose @SerializedName("current") public int current;
    @Expose @SerializedName("limit") public int limit;
    @Expose @SerializedName("items") public int items;

}
