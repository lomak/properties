package com.realestate.onliner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyContact {

    @Expose @SerializedName("contact") public boolean owner;

}
