package com.realestate.onliner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyConvertedPrice {

    @Expose @SerializedName("BYN") public PropertyPrice priceBYN;
    @Expose @SerializedName("BYR") public PropertyPrice priceBYR;
    @Expose @SerializedName("USD") public PropertyPrice priceUSD;

}
