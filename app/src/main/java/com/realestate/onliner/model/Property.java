package com.realestate.onliner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Property {

    @Expose @SerializedName("id") public int id;
    @Expose @SerializedName("price") public PropertyPrice price;
    @Expose @SerializedName("rent_type") public String rentType;
    @Expose @SerializedName("location") public PropertyLocation location;
    @Expose @SerializedName("photo") public String photo;
    @Expose @SerializedName("contact") public PropertyContact contact;
    @Expose @SerializedName("created_at") public Date createdAt;
    @Expose @SerializedName("last_time_up") public Date lastTimeUp;
    @Expose @SerializedName("up_available_in") public int upAvailableIn;
    @Expose @SerializedName("url") public String url;

}
