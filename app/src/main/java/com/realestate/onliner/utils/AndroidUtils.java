package com.realestate.onliner.utils;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class AndroidUtils {

    private static Resources sRes;
    private static Context sAppContext;

    public static void init(Context context) {
        sAppContext = context.getApplicationContext();
        sRes = sAppContext.getResources();
    }

    public static boolean isNetworkConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) sAppContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
