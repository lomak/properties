package com.realestate.onliner.injection.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.realestate.onliner.network.ApiConstants;
import com.realestate.onliner.network.DataManager;
import com.realestate.onliner.network.RestApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Provides
    @Singleton
    DataManager provideDataManager(RestApi restApi) {
        return new DataManager(restApi);
    }

    @Provides
    Gson provideGson() {
        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setDateFormat(ApiConstants.SERVER_DATE_FORMAT)
                .create();
    }

    @Provides
    @Singleton
    RestApi provideRestApi(Gson gson) {
        return new RestApi(gson);
    }
}
