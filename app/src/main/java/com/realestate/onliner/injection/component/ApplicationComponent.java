package com.realestate.onliner.injection.component;


import com.realestate.onliner.injection.module.ApplicationModule;
import com.realestate.onliner.injection.module.DataModule;
import com.realestate.onliner.ui.feed.FeedActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        DataModule.class
})
public interface ApplicationComponent {

    void inject(FeedActivity activity);

}