package com.realestate.onliner.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.realestate.onliner.R;

import java.util.List;

public abstract class BasePagingAdapter<T> extends BaseRecyclerAdapter<T, BaseRecyclerHolder> {

    protected static int ITEM_VIEW_TYPE = 1;
    protected static int LOADING_VIEW_TYPE = 0;

    public BasePagingAdapter(Context context) {
        super(context);
    }

    protected boolean allItemsLoaded = false;

    @Override
    public BaseRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LOADING_VIEW_TYPE) {
            View v = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingItemHolder(v);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        int size = super.getItemCount();
        if (size > 0 && !allItemsLoaded) {
            size++;
        }
        return size;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == items.size()) {
            return LOADING_VIEW_TYPE;
        }
        return ITEM_VIEW_TYPE;
    }

    public void addNewItems(List<T> newItems) {
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    public void setAllItemsLoaded(boolean value) {
        allItemsLoaded = value;
        notifyDataSetChanged();
    }

    public boolean isAllItemsLoaded() {
        return allItemsLoaded;
    }

    public void clearItems() {
        allItemsLoaded = false;
        items.clear();
    }

    public void clearItemsAndNotify() {
        clearItems();
        notifyDataSetChanged();
    }

    public class LoadingItemHolder extends BaseRecyclerHolder {

        public LoadingItemHolder(View itemView) {
            super(itemView);
        }
    }
}
