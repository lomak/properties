package com.realestate.onliner.ui.views;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

/**
 * Workaround for https://code.google.com/p/android/issues/detail?id=77712
 * Still exists in Support Library 24.0.0
 */
public class SwipeToRefreshLayout extends SwipeRefreshLayout {

    private boolean measured = false;
    private boolean preMeasureRefreshing = false;
    private int touchSlop;
    private float prevX;
    // Indicate if we've already declined the move event
    private boolean declined;

    public SwipeToRefreshLayout(Context context) {
        super(context);
        init(context);
    }

    public SwipeToRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        touchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!measured) {
            measured = true;
            setRefreshing(preMeasureRefreshing);
        }
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        if (measured) {
            super.setRefreshing(refreshing);
        } else {
            preMeasureRefreshing = refreshing;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                prevX = MotionEvent.obtain(event).getX();
                declined = false;
                break;

            case MotionEvent.ACTION_MOVE:
                final float eventX = event.getX();
                float dx = Math.abs(eventX - prevX);

                if (declined || dx > touchSlop) {
                    declined = true;
                    return false;
                }
        }

        return super.onInterceptTouchEvent(event);
    }

}