package com.realestate.onliner.ui.feed;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.realestate.onliner.R;
import com.realestate.onliner.model.Property;
import com.realestate.onliner.network.ErrorType;
import com.realestate.onliner.ui.BaseActivity;
import com.realestate.onliner.ui.PagingScrollListener;
import com.realestate.onliner.ui.views.EmptyRecyclerView;
import com.realestate.onliner.ui.views.SwipeToRefreshLayout;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class FeedActivity extends BaseActivity implements FeedMvpView {

    @BindView(R.id.recycler_view) EmptyRecyclerView recyclerView;
    @BindView(R.id.refresh_layout) SwipeToRefreshLayout refreshLayout;
    @BindView(R.id.network_error_view) View networkErrorView;
    @BindView(R.id.empty_view) View emptyView;

    @Inject FeedPresenter presenter;

    private LinearLayoutManager layoutManager;
    private FeedAdapter adapter;
    private PagingScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(this);
        setContentView(R.layout.activity_main);
        presenter.attachView(this);

        refreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        refreshLayout.setOnRefreshListener(() -> {
            refreshPosts();
        });

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        adapter = new FeedAdapter(this);

        scrollListener = new PagingScrollListener(new PagingScrollListener.ScrollListener() {
            @Override
            public void loadData() {
                presenter.getProperties();
            }

            @Override
            public boolean shouldLoadMore() {
                return !adapter.isAllItemsLoaded();
            }

            @Override
            public int getItemCount() {
                return layoutManager.getItemCount();
            }

            @Override
            public int getFirstVisiblePosition() {
                return layoutManager.findFirstVisibleItemPosition();
            }
        });

        recyclerView.addOnScrollListener(scrollListener);

        presenter.getProperties();
    }

    private void refreshPosts() {
        recyclerView.setEmptyView(null);
        adapter.clearItemsAndNotify();
        presenter.resetPagingParams();
        presenter.getProperties();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showFirstPage(List<Property> newItems) {
        networkErrorView.setVisibility(View.GONE);
        scrollListener.setLoading(false);
        adapter.setItems(newItems);
        recyclerView.setAdapter(adapter);
        recyclerView.setEmptyView(emptyView);
    }

    @Override
    public void showNewItems(List<Property> newItems) {
        networkErrorView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        scrollListener.setLoading(false);
        adapter.addItems(newItems);
    }

    @Override
    public void notifyAllItemsLoaded() {
        adapter.setAllItemsLoaded(true);
    }

    @Override
    public void showError(@StringRes int errorResId) {
        showToast(errorResId);
    }

    @Override
    public void showError(ErrorType errorType) {
        if (errorType == ErrorType.NETWORK_ERROR) {
            networkErrorView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showProgress() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        refreshLayout.setRefreshing(false);
    }
}
