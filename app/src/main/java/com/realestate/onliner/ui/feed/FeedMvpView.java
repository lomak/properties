package com.realestate.onliner.ui.feed;

import android.support.annotation.StringRes;

import com.realestate.onliner.model.Property;
import com.realestate.onliner.network.ErrorType;
import com.realestate.onliner.ui.PagingMvpView;

public interface FeedMvpView extends PagingMvpView<Property> {

    void showError(@StringRes int errorResId);

    void showError(ErrorType errorType);

    void showProgress();

    void hideProgress();
}
