package com.realestate.onliner.ui;

public interface Presenter<V extends MvpView> {

    void attachView(V view);

    void detachView();

}
