package com.realestate.onliner.ui.feed;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.realestate.onliner.R;
import com.realestate.onliner.model.Property;
import com.realestate.onliner.ui.BasePagingAdapter;
import com.realestate.onliner.ui.BaseRecyclerHolder;

import butterknife.BindView;

public class FeedAdapter extends BasePagingAdapter<Property> {

    private Context context;

    public FeedAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public BaseRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE) {
            View v = inflater.inflate(R.layout.item_property, parent, false);
            return new BaseItemPostHolder(v);
        }
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(BaseRecyclerHolder holder, int position) {
        if (getItemViewType(position) == ITEM_VIEW_TYPE) {
            BaseItemPostHolder baseViewHolder = (BaseItemPostHolder) holder;
            Property item = getItem(position);

            baseViewHolder.txtAddress.setText(item.location.address);

            baseViewHolder.txtPriceBYN.setText(item.price.converted.priceBYN.amount + item.price.converted.priceBYN.currency);
            baseViewHolder.txtPriceBYR.setText(item.price.converted.priceBYR.amount + item.price.converted.priceBYR.currency);
            baseViewHolder.txtPriceUSD.setText(item.price.converted.priceUSD.amount + item.price.converted.priceUSD.currency);

            baseViewHolder.txtUp.setText(String.format(context.getString(R.string.text_feed_property_up),
                    DateUtils.getRelativeTimeSpanString(item.createdAt.getTime(),
                            System.currentTimeMillis(), 0L, DateUtils.FORMAT_ABBREV_ALL)));

            baseViewHolder.txtContact.setText(item.contact.owner ? "" : context.getString(R.string.agent));

            Glide.with(context)
                    .load(item.photo)
                    .centerCrop()
                    .into(baseViewHolder.imgPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    static class BaseItemPostHolder extends BaseRecyclerHolder {

        @BindView(R.id.txt_address) TextView txtAddress;
        @BindView(R.id.txt_contact) TextView txtContact;
        @BindView(R.id.txt_price_byn) TextView txtPriceBYN;
        @BindView(R.id.txt_price_byr) TextView txtPriceBYR;
        @BindView(R.id.txt_price_usd) TextView txtPriceUSD;
        @BindView(R.id.txt_up) TextView txtUp;
        @BindView(R.id.img_photo) ImageView imgPhoto;

        public BaseItemPostHolder(View itemView) {
            super(itemView);
        }
    }
}
