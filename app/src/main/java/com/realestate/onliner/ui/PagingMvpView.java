package com.realestate.onliner.ui;

import java.util.List;

public interface PagingMvpView<T> extends MvpView {

    void showFirstPage(List<T> newItems);

    void showNewItems(List<T> newItems);

    void notifyAllItemsLoaded();
}
