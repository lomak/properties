package com.realestate.onliner.ui;

import android.support.v7.widget.RecyclerView;

public class PagingScrollListener extends RecyclerView.OnScrollListener {

    private boolean loading = false;
    private int visibleThreshold = 2;
    private int firstVisibleItem, visibleItemCount, totalItemCount;

    private ScrollListener scrollListener;

    public PagingScrollListener(ScrollListener scrollListener) {
        this.scrollListener = scrollListener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = scrollListener.getItemCount();
        firstVisibleItem = scrollListener.getFirstVisiblePosition();
        if (!loading
                && scrollListener.shouldLoadMore()
                && dy > 0
                && (visibleItemCount + firstVisibleItem) >= (totalItemCount - visibleThreshold)) {
            loading = true;
            scrollListener.loadData();
        }
    }

    public void setLoading(boolean value) {
        loading = value;
    }

    public interface ScrollListener {

        void loadData();

        boolean shouldLoadMore();

        int getItemCount();

        int getFirstVisiblePosition();
    }
}
