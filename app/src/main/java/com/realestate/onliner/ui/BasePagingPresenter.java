package com.realestate.onliner.ui;

import com.realestate.onliner.network.DataManager;

public abstract class BasePagingPresenter<V extends PagingMvpView> extends BasePresenter<V> {

    protected int page = 1;

    protected DataManager dataManager;

    public BasePagingPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public void resetPagingParams() {
        page = 1;
    }

    @Override
    public void attachView(V mvpView) {
        super.attachView(mvpView);
        resetPagingParams();
    }
}
