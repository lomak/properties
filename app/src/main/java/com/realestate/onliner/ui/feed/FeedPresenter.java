package com.realestate.onliner.ui.feed;

import com.realestate.onliner.R;
import com.realestate.onliner.network.DataManager;
import com.realestate.onliner.network.ErrorType;
import com.realestate.onliner.ui.BasePagingPresenter;
import com.realestate.onliner.utils.AndroidUtils;

import javax.inject.Inject;

import rx.Subscription;

public class FeedPresenter extends BasePagingPresenter<FeedMvpView> {

    private DataManager dataManager;

    @Inject
    public FeedPresenter(DataManager dataManager) {
        super(dataManager);
        this.dataManager = dataManager;
    }

    public void getProperties() {
        if (!AndroidUtils.isNetworkConnected()) {
            mvpView.hideProgress();
            mvpView.showError(ErrorType.NETWORK_ERROR);
            return;
        }

        if (page == 1) {
            mvpView.showProgress();
        }

        Subscription subscription = dataManager.getProperties(page)
                .subscribe(response -> {
                    if (page == 1) {
                        mvpView.showFirstPage(response.properties);
                        mvpView.hideProgress();
                    } else {
                        mvpView.showNewItems(response.properties);
                    }

                    if (response.page.limit * response.page.current >= response.total) {
                        mvpView.notifyAllItemsLoaded();
                    }

                    page++;
                }, throwable -> {
                    try {
                        mvpView.hideProgress();
                        mvpView.showError(R.string.default_error);
                    } catch (Exception e) {

                    }
                });
        subscriptions.add(subscription);
    }
}
